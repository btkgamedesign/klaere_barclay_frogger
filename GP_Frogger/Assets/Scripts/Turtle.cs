﻿using DG.Tweening;
using UnityEngine;
public class Turtle : MonoBehaviour
{
    public float yTarget;
    public float drownThreshold;
    Player player;

    void Start()
    {
        Sequence mySequence = DOTween.Sequence();
        mySequence.Append(transform.DOMoveY(yTarget, 2));
        mySequence.SetLoops(-1, LoopType.Yoyo);
        mySequence.PrependInterval(1);
    }


    void Update()
    {
        if (player != null && player.transform.position.y <= drownThreshold)
        {
            Debug.LogError("Game Over - Frog sank on turtle's back");
        }
    }
    void OnCollisionEnter(Collision collision)
    {
        Player p = collision.gameObject.GetComponent<Player>();
        if (p != null)
        {
            player = null;
        }
    }
}
