﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class Restart : MonoBehaviour
{

    public void Update()
    {
        if (Input.GetKeyDown(KeyCode.R))
        {
            Debug.Log("Restarting Game");
            SceneManager.LoadScene(SceneManager.GetActiveScene().name); // loads current scene
        }
    }

}
