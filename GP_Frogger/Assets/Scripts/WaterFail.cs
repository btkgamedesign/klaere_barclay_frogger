﻿using UnityEngine;

public class WaterFail : MonoBehaviour
{
    public GameObject SplashSource;
    AudioSource audioData;
    void Start()
    {
        audioData = SplashSource.GetComponent<AudioSource>();
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.GetComponent<Player>() != null)
        {
            if (other.transform.position.y < transform.position.y)
            {
                {
                    //gameObject.SendMessage("TeleportFroggie");   ._. rip no work
                    GameController.health -= 1;
                    Debug.Log("Frog Sank");
                    SplashSource.GetComponent<AudioSource>().Play();
                }
            }
        }
    }
}
