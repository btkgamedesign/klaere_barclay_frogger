﻿using UnityEngine;

public class GameController : MonoBehaviour
{
    public GameObject heart1, heart2, heart3, gameOver;
    public GameObject Life1, Life2, Life3;
    public static int health;
    public GameObject froggie;
    void Start()
    {
        froggie.transform.position = new Vector3(0, 0, 0);
        health = 3;
        heart1.gameObject.SetActive(true);
        heart2.gameObject.SetActive(true);
        heart3.gameObject.SetActive(true);
        gameOver.gameObject.SetActive(false);

        Life1.gameObject.SetActive(true);
        Life2.gameObject.SetActive(false);
        Life3.gameObject.SetActive(false);
    }

    void Update()
    {
        if (health > 3)
        {
            health = 3;
        }


        if (health == 3)
        {

            Life1.gameObject.SetActive(true);
            Life2.gameObject.SetActive(false);
            Life3.gameObject.SetActive(false);

            heart1.gameObject.SetActive(true);
            heart2.gameObject.SetActive(true);
            heart3.gameObject.SetActive(true);
        }
        else if (health == 2)
        {

            Life1.gameObject.SetActive(false);
            Life2.gameObject.SetActive(true);
            Life3.gameObject.SetActive(false);

            heart1.gameObject.SetActive(true);
            heart2.gameObject.SetActive(true);
            heart3.gameObject.SetActive(false);
        }
        else if (health == 1)
        {

            Life1.gameObject.SetActive(false);
            Life2.gameObject.SetActive(false);
            Life3.gameObject.SetActive(true);

            heart1.gameObject.SetActive(true);
            heart2.gameObject.SetActive(false);
            heart3.gameObject.SetActive(false);
        }
        else if (health == 0)
        {
            heart1.gameObject.SetActive(false);
            heart2.gameObject.SetActive(false);
            heart3.gameObject.SetActive(false);
            Time.timeScale = 0;
            Debug.LogError("Game Over - Ran out of lives!");
        }

    }
    //void TeleportFroggie()   [TRIED MAKING FROG GET TELEPORTED WHEN HIT BY CAR: CHECK CAR.CS]
    // Again, problem with weird coordinates upon teleportation. Seems to be a parenting issue and couldn't find workarounds :)
    //   {
    //     froggie.transform.position = new Vector3(0, 0, 0);
    //  }



}
