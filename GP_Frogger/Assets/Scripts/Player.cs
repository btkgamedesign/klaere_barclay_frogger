﻿using DG.Tweening;
using UnityEngine;
public class Player : MonoBehaviour
{
    //private static Player _instance;
    //public static Player Instance { get { return _instance; } }


    Vector3 forward = new Vector3(1, 0, 0);
    Vector3 left = new Vector3(0, 0, 1);
    public Transform nWall;
    public Transform sWall;
    public Transform wWall;
    public Transform eWall;
    public Transform HomeWall1;
    public Transform HomeWall2;
    public Transform HomeWall3;
    public Transform HomeWall4;
    public Transform HomeWall5;
    public Transform HomeWall6;
    public Vector3 target;


    bool IsMoving;

    //private void Awake()
    //{
    //    _instance = this;
    //}

    //public void ReachedHome()
    //{

    //    // create Frog plox
    //    Instantiate(FrogPrefab, transform.position, Quaternion.identity);
    //    transform.position = new Vector3(0, 0, 0);
    //    target = new Vector3(0, 0, 0);
    //}

    void Update()
    {
        Vector3 target = transform.position;

        if (!IsMoving)
        {


            if (Input.GetKeyUp(KeyCode.W))
            {
                target += forward;
                target.Set(Mathf.Min(target.x, nWall.position.x - 1), target.y, target.z);

            }
            else if (Input.GetKeyUp(KeyCode.S))
            {
                target -= forward;
                target.Set(Mathf.Max(target.x, sWall.position.x + 1), target.y, target.z);

            }
            else if (Input.GetKeyUp(KeyCode.A))
            {
                target += left;
                target.Set(target.x, target.y, Mathf.Min(target.z, wWall.position.z - 1));

            }
            else if (Input.GetKeyUp(KeyCode.D))
            {
                target -= left;
                target.Set(target.x, target.y, Mathf.Max(target.z, eWall.position.z + 1));

            }

            // This is where the HomeWall limitations come in. Oh boy, they were fun to make.

            //FORWARD HOP
            else if (Input.GetKeyUp(KeyCode.W))
            {
                target += forward;
                target.Set(Mathf.Min(target.x, HomeWall1.position.x - 1), target.y, (HomeWall1.position.z));

            }
            else if (Input.GetKeyUp(KeyCode.W))
            {
                target += forward;
                target.Set(Mathf.Min(target.x, HomeWall2.position.x - 1), target.y, (HomeWall2.position.z));

            }
            else if (Input.GetKeyUp(KeyCode.W))
            {
                target += forward;
                target.Set(Mathf.Min(target.x, HomeWall3.position.x - 1), target.y, (HomeWall3.position.z));

            }
            else if (Input.GetKeyUp(KeyCode.W))
            {
                target += forward;
                target.Set(Mathf.Min(target.x, HomeWall4.position.x - 1), target.y, (HomeWall4.position.z));

            }
            else if (Input.GetKeyUp(KeyCode.W))
            {
                target += forward;
                target.Set(Mathf.Min(target.x, HomeWall5.position.x - 1), target.y, (HomeWall5.position.z));

            }
            else if (Input.GetKeyUp(KeyCode.W))
            {
                target += forward;
                target.Set(Mathf.Min(target.x, HomeWall6.position.x - 1), target.y, (HomeWall6.position.z));

            }

        }


        if (Vector3.SqrMagnitude(transform.position - target) > 0.01f)
        {
            transform.DOMove(target, .1f).OnComplete(() => IsMoving = false);
        }

    }
}
