﻿using UnityEngine;

public class InGameQuit : MonoBehaviour
{
    public void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            Debug.Log("Goodbye! Application Quitting...");
            Application.Quit();
        }
    }
}
