﻿using UnityEngine;

public class Coin : MonoBehaviour
{
    static int pickedUp;
    public GameObject MoneyCollector;
    void Start()
    {
        Rigidbody rb = GetComponent<Rigidbody>();
        rb.angularVelocity = new Vector3(0, 3, 0);
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.GetComponent<Player>() != null)
        {
            ScoreScript.scoreValue += 10;
            gameObject.SetActive(false);
            pickedUp++;
            MoneyCollector.GetComponent<AudioSource>().Play();
            Debug.Log("Ka-ching! " + pickedUp + " coin(s) collected!");
        }
    }
}
