﻿using UnityEngine;

public class Car : MonoBehaviour
{
    public bool IsMovingLeft;
    public float speedFactor;
    Vector3 movement;
    Transform player;
    public GameObject CarSquashie;


    void Start()
    {
        if (IsMovingLeft)
        {
            movement = new Vector3(0, 0, 1);
        }
        else
        {
            movement = new Vector3(0, 0, -1);
        }
    }

    void Update()
    {
        Vector3 move = movement * speedFactor * Time.deltaTime;
        transform.position += move;
        if (player != null)
        {
            player.position += move;
        }

    }

    void OnTriggerExit(Collider other)
    {
        if (other.GetComponent<Wall>() != null)
        {
            transform.position = transform.position + new Vector3(0, 0, -other.transform.position.z * 2);
        }
    }

    void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.GetComponent<Player>() != null)
        {
            //gameObject.SendMessage("TeleportFroggie");
            GameController.health -= 1;
            Debug.Log("Car Accident");
            CarSquashie.GetComponent<AudioSource>().Play();
        }
    }
}
