﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HighScoresManager : MonoBehaviour
{
    public GameObject linePrefab;
    List<GameObject> lines;
    public int scoresToShow;
    public static HighScoresManager Instance;

    void Awake()
    {
        Instance = this;
        lines = new List<GameObject>();
    }

    public void PopulateScoreTable()
    {
        for (int s = 0; s < scoresToShow; s++)
        {
            string scoreKey = $"score{s}";
            if (PlayerPrefs.HasKey(scoreKey))
            {
                float score = PlayerPrefs.GetInt(scoreKey);
                GameObject newLine = Instantiate(linePrefab, transform);
                newLine.GetComponent<Text>().text = $"{score}";
            }
        }
    }

    public void EnterNewScore(int newScore)
    {
        // get old scores
        List<int> scores = new List<int>();
        for (int s = 0; s < scoresToShow; s++)
        {
            string scoreKey = $"score{s}";
            if (PlayerPrefs.HasKey(scoreKey))
            {
                scores.Add(PlayerPrefs.GetInt(scoreKey));
            }
        }
        // add new score
        scores.Add(newScore);

        // Sort scores from low to high
        scores.Sort();

        // now it's high to low
        scores.Reverse();

        int howManyScores = Mathf.Min(scores.Count, scoresToShow);
        for (int s = 0; s < howManyScores; s++)
        {
            string scoreKey = $"score{s}";
            PlayerPrefs.SetInt(scoreKey, scores[s]);
        }
    }

    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            while (lines.Count > 0)
            {
                GameObject l = lines[0];
                lines.RemoveAt(0);
                Destroy(l);
            }
            StateManager.Instance.RestartGame();
        }
    }
}
