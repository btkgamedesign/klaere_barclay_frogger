﻿using UnityEngine;

public class CameraTarget : MonoBehaviour
{
    public Transform player;
    Vector3 shift;

    void Start()
    {
        shift = transform.position - player.position;
    }

    void Update()
    {
        transform.position = shift + player.position;
    }

}
