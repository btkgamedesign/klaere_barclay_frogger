﻿using UnityEngine;

public class Lily : MonoBehaviour
{
    public bool IsMovingLeft;
    Vector3 movement;
    Transform player;

    void Start()
    {
        if (IsMovingLeft)
        {
            movement = new Vector3(0, 0, 1);
        }
        else
        {
            movement = new Vector3(0, 0, -1);
        }
    }

    void Update()
    {
        Vector3 move = movement * Time.deltaTime;
        transform.position += move;
        if (player != null)
        {
            player.position += move;
        }
    }
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.GetComponent<Player>() != null)
        {
            player = collision.transform;
        }
    }
    private void OnCollisionExit(Collision collision)
    {
        if (collision.gameObject.GetComponent<Player>() != null)
        {
            player = null;
        }
    }

    float lastWarped;
    void OnTriggerExit(Collider other)
    {
        if (other.GetComponent<Wall>() != null)
        {
            float now = Time.time;
            if (now > lastWarped + 2)
            {
                lastWarped = now;


                transform.position = transform.position + new Vector3(0, 0, -other.transform.position.z * 2);
            }
        }
    }
}
