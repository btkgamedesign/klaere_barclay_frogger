﻿using UnityEngine;
using UnityEngine.SceneManagement;
public class VictoryL4 : MonoBehaviour
{
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.GetComponent<Player>() != null)
        {
            Debug.Log("Victory!");
            ScoreScript.scoreValue += 100;
            SceneManager.LoadScene("Level5");
        }
    }
}
